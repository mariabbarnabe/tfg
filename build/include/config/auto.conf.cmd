deps_config := \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/app_update/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/aws_iot/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/esp8266/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/esp_http_client/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/esp_http_server/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/freertos/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/libsodium/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/log/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/lwip/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/mdns/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/mqtt/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/newlib/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/pthread/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/ssl/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/tcpip_adapter/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/wifi_provisioning/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/wpa_supplicant/Kconfig \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/bootloader/Kconfig.projbuild \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/esptool_py/Kconfig.projbuild \
	/home/maria/esp8266/ESP8266_RTOS_SDK/examples/get-started/architect1/main/Kconfig.projbuild \
	/home/maria/esp8266/ESP8266_RTOS_SDK/components/partition_table/Kconfig.projbuild \
	/home/maria/esp8266/ESP8266_RTOS_SDK/Kconfig

include/config/auto.conf: \
	$(deps_config)


$(deps_config): ;
