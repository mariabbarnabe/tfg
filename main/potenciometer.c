#include "potenciometer.h"
#include "driver/adc.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_event_loop.h"

#include "database.h"

static const char *TAG = "example";

void readPotenciometer(void *prm)
{           
    //ESP_LOGI(TAG, "lePotenciometro");
    uint16_t read;
    while(1)
    {
      //  ESP_LOGI(TAG, "while");
        read = adc_read();
        //ESP_LOGI(TAG, "lê da porta");
        setData(DB_POT, &read);
        //ESP_LOGI(TAG, "potenciometro val: %i\n", leitura);
        vTaskDelay(100 / portTICK_RATE_MS);

    }
    
}