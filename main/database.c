#include <stdint.h>
#include "database.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "esp_log.h"


#define varCreate(nome) xQueueHandle led2Queue = NULL;

xQueueHandle data[DB_END];

const int size[DB_END]= SIZE_LIST;

static const char *TAG = "example";

void setData(int nome, void * valor) 
{
    xQueueOverwrite(data[nome], valor);
}

void  getData(int nome, void * valor)
{    
    xQueuePeek(data[nome],valor, portMAX_DELAY);
}

void initBD(void)
{
    int dummy;
    ESP_LOGI(TAG, "initBD start");
    for(int i=0;i<DB_END;i++){
        data[i] = xQueueCreate(1, size[i]);
        dummy = 1;
        xQueueSend(data[i], ( void * ) &dummy, portMAX_DELAY);
    }
    ESP_LOGI(TAG, "initBD finish");
}
