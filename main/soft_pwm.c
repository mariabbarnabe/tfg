/* BSD Socket API Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "soft_pwm.h"
#include "database.h"
#include "esp_system.h"
#include "esp_log.h"

#define GPIO_OUTPUT_IO_0    15
#define GPIO_OUTPUT_PIN_SEL  (1ULL<<GPIO_OUTPUT_IO_0)

void soft_pwm()
{
    uint16_t read;
    while(1)
    {
        getData(DB_POT, &read);
        gpio_set_level(GPIO_OUTPUT_IO_0, 1);
	    vTaskDelay((1023-read) / portTICK_RATE_MS/30);
	    gpio_set_level(GPIO_OUTPUT_IO_0, 0);
	    vTaskDelay(read / portTICK_RATE_MS/30);
    }
}
