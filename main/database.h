#ifndef DATABASE_H
#define DATABASE_H

void setData(int nome, void * valor);
void getData(int nome, void * valor);

void initBD(void);

enum {DB_POT, DB_LED, DB_END};
#define SIZE_LIST {sizeof(int), sizeof(int)}

#endif