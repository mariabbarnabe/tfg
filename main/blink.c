/* BSD Socket API Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "blink.h"
#include "database.h"
#include "esp_system.h"
#include "esp_log.h"

#define GPIO_OUTPUT_IO_1    16
#define GPIO_OUTPUT_PIN_SEL  (1ULL<<GPIO_OUTPUT_IO_1)

void blink()
{
    uint16_t read;
    uint16_t ledValue;
    while(1)
    {
        getData(DB_POT, &read);
	    gpio_set_level(GPIO_OUTPUT_IO_1, 1);
        ledValue = gpio_get_level(GPIO_OUTPUT_IO_1);
        setData(DB_LED, &ledValue);
	    vTaskDelay(read / portTICK_RATE_MS);
        gpio_set_level(GPIO_OUTPUT_IO_1, 0);
        ledValue = gpio_get_level(GPIO_OUTPUT_IO_1);
        setData(DB_LED, &ledValue);
	    vTaskDelay(read / portTICK_RATE_MS);
        
    }
}